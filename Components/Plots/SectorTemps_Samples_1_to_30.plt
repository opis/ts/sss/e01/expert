<?xml version="1.0" encoding="UTF-8"?>
<databrowser>
  <title></title>
  <show_legend>true</show_legend>
  <show_toolbar>true</show_toolbar>
  <update_period>3.0</update_period>
  <scroll_step>5</scroll_step>
  <scroll>false</scroll>
  <start>2022-12-21 11:43:00.000</start>
  <end>2022-12-21 11:49:00.000</end>
  <archive_rescale>STAGGER</archive_rescale>
  <foreground>
    <red>0</red>
    <green>0</green>
    <blue>0</blue>
  </foreground>
  <background>
    <red>255</red>
    <green>255</green>
    <blue>255</blue>
  </background>
  <title_font>Liberation Sans|20|1</title_font>
  <label_font>Liberation Sans|14|1</label_font>
  <scale_font>Liberation Sans|12|0</scale_font>
  <legend_font>Liberation Sans|14|0</legend_font>
  <axes>
    <axis>
      <visible>true</visible>
      <name>Temperature [degC]</name>
      <use_axis_name>true</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>30.0</min>
      <max>211.0</max>
      <grid>false</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
  </axes>
  <annotations>
  </annotations>
  <pvlist>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_01</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_01</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_02</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_02</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_03</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_03</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_04</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_04</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_05</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_05</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_06</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_06</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_07</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_07</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_08</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_08</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_09</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_09</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_10</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_10</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_11</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_11</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_12</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_12</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_13</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_13</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_14</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_14</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_15</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_15</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_16</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_16</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_17</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_17</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_18</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_18</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_19</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_19</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_20</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_20</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_21</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_21</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_22</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_22</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_23</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_23</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_24</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_24</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_25</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_25</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_26</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_26</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_27</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_27</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_28</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_28</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>51</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_29</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_29</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
    <pv>
      <display_name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_30</display_name>
      <visible>true</visible>
      <name>Tgt-TWDS1000:Mech-Sect-34:T_Cup_Samp_30</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
  </pvlist>
</databrowser>

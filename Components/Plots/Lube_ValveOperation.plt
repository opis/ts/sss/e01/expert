<?xml version="1.0" encoding="UTF-8"?>
<databrowser>
  <title></title>
  <show_legend>true</show_legend>
  <update_period>3.0</update_period>
  <scroll_step>5</scroll_step>
  <scroll>true</scroll>
  <start>-1 hours</start>
  <end>now</end>
  <archive_rescale>STAGGER</archive_rescale>
  <foreground>
    <red>0</red>
    <green>0</green>
    <blue>0</blue>
  </foreground>
  <background>
    <red>230</red>
    <green>230</green>
    <blue>230</blue>
  </background>
  <title_font>Liberation Sans|16|1</title_font>
  <label_font>Liberation Sans|10|1</label_font>
  <scale_font>Liberation Sans|10|0</scale_font>
  <legend_font>Liberation Sans|14|0</legend_font>
  <axes>
    <axis>
      <visible>false</visible>
      <name>Value 0</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>-8.01</min>
      <max>1.6</max>
      <grid>false</grid>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
    </axis>
    <axis>
      <visible>false</visible>
      <name>Value 1</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>0.0</min>
      <max>10.0</max>
      <grid>false</grid>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
    </axis>
  </axes>
  <annotations>
    <annotation>
      <pv>-1</pv>
      <time>2022-01-14 19:42:17.805</time>
      <value>1.0</value>
      <offset>
        <x>20.0</x>
        <y>20.0</y>
      </offset>
      <text>{0}, {2}</text>
    </annotation>
    <annotation>
      <pv>-1</pv>
      <time>2022-01-14 19:42:17.805</time>
      <value>1.0</value>
      <offset>
        <x>20.0</x>
        <y>20.0</y>
      </offset>
      <text>{0}, {2}</text>
    </annotation>
    <annotation>
      <pv>-1</pv>
      <time>2022-01-14 19:42:17.805</time>
      <value>1.0</value>
      <offset>
        <x>20.0</x>
        <y>20.0</y>
      </offset>
      <text>{0}, {2}</text>
    </annotation>
    <annotation>
      <pv>-1</pv>
      <time>2022-01-14 19:42:17.805</time>
      <value>1.0</value>
      <offset>
        <x>20.0</x>
        <y>20.0</y>
      </offset>
      <text>{0}, {2}</text>
    </annotation>
  </annotations>
  <pvlist>
    <pv>
      <display_name>$(SystemName):$(DeviceDis)-$(DeviceTag):Valve_Ctrl_Open</display_name>
      <visible>true</visible>
      <name>$(SystemName):$(DeviceDis)-$(DeviceTag):Valve_Ctrl_Open</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>51</green>
        <blue>51</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://172.30.6.170:17668/retrieval</name>
        <url>pbraw://172.30.6.170:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>$(SystemName):$(DeviceDis)-$(DeviceTag):Ctrl_DO_Flt</display_name>
      <visible>true</visible>
      <name>$(SystemName):$(DeviceDis)-$(DeviceTag):Ctrl_DO_Flt</name>
      <axis>1</axis>
      <color>
        <red>0</red>
        <green>51</green>
        <blue>51</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://172.30.6.170:17668/retrieval</name>
        <url>pbraw://172.30.6.170:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
  </pvlist>
</databrowser>

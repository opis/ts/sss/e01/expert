<?xml version="1.0" encoding="UTF-8"?>
<databrowser>
  <title></title>
  <show_legend>true</show_legend>
  <show_toolbar>true</show_toolbar>
  <grid>true</grid>
  <update_period>3.0</update_period>
  <scroll_step>5</scroll_step>
  <scroll>true</scroll>
  <start>-1 hours</start>
  <end>now</end>
  <archive_rescale>NONE</archive_rescale>
  <foreground>
    <red>0</red>
    <green>0</green>
    <blue>0</blue>
  </foreground>
  <background>
    <red>242</red>
    <green>242</green>
    <blue>242</blue>
  </background>
  <title_font>Liberation Sans|19|1</title_font>
  <label_font>Liberation Sans|21|1</label_font>
  <scale_font>Liberation Sans|18|0</scale_font>
  <legend_font>Liberation Sans|18|0</legend_font>
  <axes>
    <axis>
      <visible>true</visible>
      <name>Displacement RMS Mean [um]</name>
      <use_axis_name>true</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>-6.5</min>
      <max>76.3</max>
      <grid>true</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
  </axes>
  <annotations>
  </annotations>
  <pvlist>
    <pv>
      <display_name>BA-011x</display_name>
      <visible>true</visible>
      <name>Tgt-TMS1063:Proc-BA-011x:Disp_RMS_Mean</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>128</green>
        <blue>128</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>BA-011y</display_name>
      <visible>true</visible>
      <name>Tgt-TMS1063:Proc-BA-011y:Disp_RMS_Mean</name>
      <axis>0</axis>
      <color>
        <red>77</red>
        <green>102</green>
        <blue>204</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>BA-012x</display_name>
      <visible>true</visible>
      <name>Tgt-TMS1063:Proc-BA-012x:Disp_RMS_Mean</name>
      <axis>0</axis>
      <color>
        <red>102</red>
        <green>77</green>
        <blue>179</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>BA-012y</display_name>
      <visible>true</visible>
      <name>Tgt-TMS1063:Proc-BA-012y:Disp_RMS_Mean</name>
      <axis>0</axis>
      <color>
        <red>128</red>
        <green>77</green>
        <blue>128</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>BA-021</display_name>
      <visible>true</visible>
      <name>Tgt-TMS1063:Proc-BA-021:Disp_RMS_Mean</name>
      <axis>0</axis>
      <color>
        <red>179</red>
        <green>102</green>
        <blue>128</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>BA-022</display_name>
      <visible>true</visible>
      <name>Tgt-TMS1063:Proc-BA-022:Disp_RMS_Mean</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>102</green>
        <blue>102</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>BA-023</display_name>
      <visible>true</visible>
      <name>Tgt-TMS1063:Proc-BA-023:Disp_RMS_Mean</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>153</green>
        <blue>128</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>BA-024</display_name>
      <visible>true</visible>
      <name>Tgt-TMS1063:Proc-BA-024:Disp_RMS_Mean</name>
      <axis>0</axis>
      <color>
        <red>204</red>
        <green>128</green>
        <blue>51</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
  </pvlist>
</databrowser>

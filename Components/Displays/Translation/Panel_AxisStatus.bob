<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2025-03-07 16:12:21 by evanfoy-->
<display version="2.0.0">
  <name>Display</name>
  <width>580</width>
  <height>270</height>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_Panel</name>
    <width>580</width>
    <height>270</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_Panel</name>
    <class>HEADER3</class>
    <text>Axis Status</text>
    <x>20</x>
    <width>140</width>
    <height>50</height>
    <font use_class="true">
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED Error</name>
    <pv_name>Tgt-TWDS1000:MC-SC-$(idx):Error</pv_name>
    <x>320</x>
    <y>220</y>
    <off_color>
      <color name="OFF" red="90" green="110" blue="90">
      </color>
    </off_color>
    <on_color>
      <color name="MAJOR" red="252" green="13" blue="27">
      </color>
    </on_color>
    <font>
      <font family="Source Code Pro Semibold" style="REGULAR" size="29.0">
      </font>
    </font>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED_1</name>
    <pv_name>Tgt-TWDS1000:MC-SC-$(idx):Moving</pv_name>
    <x>60</x>
    <y>60</y>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED_2</name>
    <pv_name>Tgt-TWDS1000:MC-SC-$(idx):Stopping</pv_name>
    <x>60</x>
    <y>100</y>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED_3</name>
    <pv_name>Tgt-TWDS1000:MC-SC-$(idx):Standstill</pv_name>
    <x>60</x>
    <y>140</y>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED_4</name>
    <pv_name>Tgt-TWDS1000:MC-SC-$(idx):Homing</pv_name>
    <x>60</x>
    <y>180</y>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED_5</name>
    <pv_name>Tgt-TWDS1000:MC-SC-$(idx):HomingAtSwitch</pv_name>
    <x>60</x>
    <y>220</y>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_1</name>
    <pv_name>Tgt-TWDS1000:MC-SC-$(idx):ErrorID</pv_name>
    <x>420</x>
    <y>170</y>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_2</name>
    <pv_name>Tgt-TWDS1000:MC-SC-$(idx):ErrorID</pv_name>
    <x>420</x>
    <y>60</y>
  </widget>
  <widget type="bool_button" version="2.0.0">
    <name>Boolean Button_1</name>
    <pv_name>Tgt-TWDS1000:MC-SC-$(idx):Cmd_Reset</pv_name>
    <x>410</x>
    <y>215</y>
    <width>120</width>
    <off_label>Reset</off_label>
    <off_color>
      <color name="Button_Background" red="236" green="236" blue="236">
      </color>
    </off_color>
    <on_label>Stop</on_label>
    <on_color>
      <color name="Button_Background" red="236" green="236" blue="236">
      </color>
    </on_color>
    <show_led>false</show_led>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <mode>1</mode>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>Moving</text>
    <x>90</x>
    <y>50</y>
    <height>40</height>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_2</name>
    <text>Stopping</text>
    <x>90</x>
    <y>90</y>
    <height>40</height>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_3</name>
    <text>Standstill</text>
    <x>90</x>
    <y>130</y>
    <height>40</height>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_4</name>
    <text>Homing</text>
    <x>90</x>
    <y>170</y>
    <height>40</height>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_5</name>
    <text>Homing At Switch</text>
    <x>90</x>
    <y>210</y>
    <width>140</width>
    <height>40</height>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_ErrorID</name>
    <text>Error ID:</text>
    <x>340</x>
    <y>170</y>
    <width>70</width>
    <horizontal_alignment>2</horizontal_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_Torq</name>
    <text>Relative Torque:</text>
    <x>280</x>
    <y>60</y>
    <width>130</width>
    <horizontal_alignment>2</horizontal_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_Error</name>
    <text>Error</text>
    <x>350</x>
    <y>210</y>
    <width>60</width>
    <height>40</height>
    <vertical_alignment>1</vertical_alignment>
  </widget>
</display>
